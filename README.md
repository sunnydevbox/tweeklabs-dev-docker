# Tweeklabs Dev Environment

Docker running Nginx, PHP-FPM 5.6/7.x, Composer, MySQL

## Initialize the container

1. Install Docker

2. In Terminal, cd into this folder

3. On the first run, run 
    ```sh
        docker-compose up -d --build
    ```
    Wait for it to finish pulling the images
4. Copy `.env.example` to `.env`. The default settings should be good enough to start
___

## Begin Development

### NGINX
1. Create an entry in your /etc/hosts (Mac/Linux) file.
    
    Example:
    
    127.0.0.1       `personal-gourmet`.localhost
2. Copy the content of /this/docker/working/directory/`etc/nginx/default.conf` and create NGINX conf into /this/docker/working/directory/`etc/nginx/conf.d/personal-gourmet.conf`

3. Modify the directory path of your project
4. Modify `fastcgi_pass php56:9000`; // This will utilize PHP 5.6
    
    `fastcgi_pass   php:9000;` // If your project requires PHP 7.x

### Project Files

1. Copy or Git pull your project inside /this/docker/working/directory/var/www/`personal-gourmet`/

2. Configure your database config (.env) with the ff:
```sh
    DB_HOST=localhost
    DB_PORT=3306
    DB_DATABASE=`this will be the db you will create later on`
    DB_USERNAME=root
    DB_PASSWORD=root
```


### Mysql Client
1. In your Mysql Client (Sequel Pro, PhpMyAdmin, etc), use these settings
    ```sh
        host=localhost
        port=8989
        username=root
        password=root
    ```

### Log in into bash

#### PHP 5.6
    docker exec -it tweeklabs-php56 bash
    
#### PHP 7
    docker exec -it tweeklabs-php bash

#### NGINX
    docker exec -it tweeklabs-nginx bash

#### Mysql
    docker exec -it tweeklabs-mysql57 bash



### Composer
In order to run composer update/install/etc, you must login to their respective PHP containers as listed above

